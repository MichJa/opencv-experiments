import numpy as np
import cv2
import glob

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.0001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((7*6,3), np.float32)
objp[:,:2] = np.mgrid[0:7,0:6].T.reshape(-1,2)

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

#images = glob.glob('*.jpg')
images = glob.glob('IMG_9651bw.jpg')
img = cv2.imread(images[0])

height, width = img.shape[:2]
scaling = 3

imgS = cv2.resize(img,(width/scaling, height/scaling), interpolation = cv2.INTER_CUBIC)
gray = cv2.cvtColor(imgS,cv2.COLOR_BGR2GRAY)

# Find the chess board corners
ret, corners = cv2.findChessboardCorners(gray, (7,6), None)

print("does it work? " + str(ret))

if ret:
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    corners2 = corners*scaling
    cv2.cornerSubPix(gray,corners2,(11,11),(-1,-1),criteria)
    
    objpoints.append(objp)
    imgpoints.append(corners2)
    
    # Draw and display the corners
    cv2.drawChessboardCorners(img, (7,6), corners2,ret)
    imgD = cv2.resize(img,(800, 600), interpolation = cv2.INTER_CUBIC)
    cv2.imshow('img',imgD)
    cv2.imwrite(images[0],img)
    cv2.waitKey(50000)
    
    cv2.destroyAllWindows()


#images = glob.glob('*.jpg')
#
#for fname in images:
#    img = cv2.imread(fname)
#    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
#
#    # Find the chess board corners
#    ret, corners = cv2.findChessboardCorners(gray, (7,6),None)
#
#    # If found, add object points, image points (after refining them)
#    if ret == True:
#        objpoints.append(objp)
#
#        corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
#        imgpoints.append(corners2)
#
#        # Draw and display the corners
#        img = cv2.drawChessboardCorners(img, (7,6), corners2,ret)
#        cv2.imshow('img',img)
#        cv2.waitKey(500)
#
#cv2.destroyAllWindows()
#cv2.destroyAllWindows()



