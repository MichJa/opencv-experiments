

import cv2
import numpy as np
from matplotlib import pyplot as plt
import Tkinter
from PIL import Image, ImageTk
from sys import argv

rsltsize = 400
margin = 200

# read and resize image
img = cv2.imread('test-3.jpg')
height, width = img.shape[:2]
img = cv2.resize(img,(800, 600), interpolation = cv2.INTER_CUBIC)

image = Image.fromarray(img)

# display image
window = Tkinter.Tk(className="Select reference points")
canvas = Tkinter.Canvas(window, width=image.size[0], height=image.size[1])
canvas.pack()
image_tk = ImageTk.PhotoImage(image)
canvas.create_image(image.size[0]//2, image.size[1]//2, image=image_tk)

refpoints = []
counter = 1

def callback(event):
    global result, counter
    print "clicked at: ", event.x, event.y
    refpoints.append([event.x, event.y])
    counter += 1
    if counter > 4:
        window.destroy()

canvas.bind("<Button-1>", callback)
Tkinter.mainloop()

rows,cols,ch = img.shape
pts1 = np.float32(refpoints)
pts2 = np.float32([[margin,margin],[rsltsize+margin,margin],[rsltsize+margin,rsltsize+margin],[margin,rsltsize+margin]])
M = cv2.getPerspectiveTransform(pts1,pts2)
dst = cv2.warpPerspective(img,M,(rsltsize+2*margin,rsltsize+2*margin))

cv2.imwrite("test-transformed-3.jpg", dst)

plt.subplot(121),plt.imshow(img),plt.title('Input')
plt.subplot(122),plt.imshow(dst),plt.title('Output')
plt.show()



